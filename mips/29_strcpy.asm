.data
x: .byte 0-512
y: .asciiz "hello"

.text

j Main

sanag_strcpy:
# Assume that base addresses for arrays x and y are found in $a0 and $a1
# while i is in $s0

subi $sp,$sp,4		# adjust stack for 1 more item
sw $s0, 0($sp)		# save $s0

add $s0,$zero,$zero	# i = 0 + 0

Loop:
	add $t1,$s0,$a1	# address of y[i] in $t1
	lbu $t2, 0($t1)	# $t2 = y[i]
	
	add $t3,$s0,$a0	# address of x[i] in $t3
	sb $t2, 0($t3)	# x[i] = y[i]
	
	beq $t2,$zero,Exit 	# if y[i] == 0, exit loop
	
	addi $s0, $s0,1 	# increment i
	j Loop 			# loop back
Exit:
lw $s0, 0($sp)		# y[i] == 0: end of string.
			# Restore old $s0
addi $sp,$sp,4 		# pop 1 word off stack
jr $ra 			# return

# main program
Main:

# load address of x and y
la $a0, x
la $a1, y

jal sanag_strcpy

# $a0 already contains the x array
li $v0, 4	#specifying print service
syscall