# left shift
li $s0, 9
sll $t2, $s0, 4

# loading
li $t2, 0xdc0
li $t1, 0x3c00

# AND
and $t0, $t1, $t2

# OR
or $t0, $t1, $t2

# NOR 
li $t3, 0
nor $t0, $t1, $t3
