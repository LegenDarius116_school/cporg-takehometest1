.data
i: .word 1
j_v: .word 2
f: .word 3
g: .word 6
h: .word 22

.text
lw $s0, i
lw $s1, j_v
lw $s2, f
lw $s3, g
lw $s4, h

bne $s0, $s1, Else
	add $s2, $s3, $s4
	j Exit
Else:
	sub $s2, $s3, $s4
Exit:
