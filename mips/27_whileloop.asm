.data
h: .word 25
save: .word 0-100 
size: .word 100

.text
la $s6, save

addi $s5, $zero, 25	# int k = 25
add $s3, $zero, $zero	# int i = 0

# loading some values into array
lw $s5, 0($s6)
lw $s5, 4($s6)
lw $s5, 8($s6)
lw $s5, 12($s6)

Loop:
	sll $t1, $s3, 2		# solve byte addressing problem
	add $t1, $t1, $s6	# get address of save[i]
	lw $t0, 0($t1)		# load save[i] into temp register
	
	bne $t0, $s5, Exit	# break out of loop if condition is not met
	
	addi $s3, $s3, 1	# increment i
	j Loop			# loop around
Exit:
