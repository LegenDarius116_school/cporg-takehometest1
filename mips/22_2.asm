.data
f: .word 0
g: .word 50 
#h: .word 40 
#i: .word 30 
#j: .word 20 

.text
lw $s0, f 
lw $s1, g 

addi $t5, $zero, 40
addi $t6, $zero, 30
addi $t7, $zero, 20

# allocate memory on stack
subi $sp, $fp, 12

# push local variables on stack
sw $t5, -4($fp)
sw $t6, -8($fp)
sw $t7, -12($fp)

# read local variables on stack
lw $s2, -4($fp)
lw $s3, -8($fp)
lw $s4, -12($fp)

# t0 = g+h
add $t0, $s1, $s2

# t1 = i+j
add $t1, $s3, $s4

# f = t0 - t1
sub $s0, $t0, $t1
sw $s0, f
