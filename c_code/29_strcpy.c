// from the MIPS textbook, section 2.9
// copies string y to string x
void sanag_strcpy (char x[], char y[])
{
	int i;
	i = 0;
	while ((x[i] = y[i]) != '\0') /* copy & test byte */
		i += 1;
}

int main() {

	char x[512];
	char* y = "hello";

	sanag_strcpy(x, y);

	return 0;
}