import std.stdio : writeln;
import std.process;

immutable string[] files = [
	"22_1",
	"22_2",
	"23_1",
	"23_2",
	"25_2",
	"26_1",
	"27_whileloop",
	"27_forloop",
	"27_ifelse",
	"29_strcpy"
];

void main() {

	foreach(string s ; files) {
		auto pid = spawnShell("gcc -g -o0 " ~ s ~ ".c -o " ~ s);
		
		if (wait(pid) != 0)
   			writeln("Compilation failed.");
	}

}