This is the readme for the Take Home Test 1, Computer Organization Fall 2018.
This Take Home Test is by Darius San Agustin, for Professor Gertner.

To use the MIPS code, simply open each file in MARS Simulator (the version I used was MARS 4.5), assemble, and then run.

For the C code, I compiled them using this command: 

gcc -g -o0 <filename>.c -o <exename>, 

where filename is the name of the source code file for c and exename is whatever program name you want. 

To make this easier, I wrote another program in D that runs that command on all of the source files in the directory. This allows me to run just one program to compile all source files, but it is not necessarily needed.